#!/usr/bin/env python
# coding : utf-8

"""
author : Arnaud Sibenaler
"""


class SimpleComplexCalculator:
    """classe SimpleComplexCalculator"""

    def __init__(self):
        """Constructeur"""

    def fsum(self, a, b):
        """somme des attributs complexes"""
        if (
            (isinstance(a, list))
            & (isinstance(b, list))
            & (len(a) == len(b) == 2)
            & ((type(a[0]) in [int, float]))
            & ((type(a[1]) in [int, float]))
            & ((type(b[0]) in [int, float]))
            & ((type(b[1]) in [int, float]))
        ):
            return [
                a[0] + b[0],
                a[1] + b[1],
            ]
        return "ERROR"

    def substract(self, a, b):
        """difference des attributs complexes"""
        if (
            (isinstance(a, list))
            & (isinstance(b, list))
            & (len(a) == len(b) == 2)
            & ((type(a[0]) in [int, float]))
            & ((type(a[1]) in [int, float]))
            & ((type(b[0]) in [int, float]))
            & ((type(b[1]) in [int, float]))
        ):
            return [
                a[0] - b[0],
                a[1] - b[1],
            ]
        return "ERROR"

    def multiply(self, a, b):
        """produit des attributs complexes"""
        if (
            (isinstance(a, list))
            & (isinstance(b, list))
            & (len(a) == len(b) == 2)
            & ((type(a[0]) in [int, float]))
            & ((type(a[1]) in [int, float]))
            & ((type(b[0]) in [int, float]))
            & ((type(b[1]) in [int, float]))
        ):
            return [
                a[0] * b[0] - a[1] * b[1],
                a[0] * b[1] + a[1] * b[0],
            ]
        return "ERROR"

    def divide(self, a, b):
        """quotient des attributs complexes"""
        if (
            (isinstance(a, list))
            & (isinstance(b, list))
            & (len(a) == len(b) == 2)
            & ((type(a[0]) in [int, float]))
            & ((type(a[1]) in [int, float]))
            & ((type(b[0]) in [int, float]))
            & ((type(b[1]) in [int, float]))
        ):
            if (b[0] ** 2 + b[1] ** 2) != 0.0:
                return [
                    (a[0] * b[0] + a[1] * b[1]) / (b[0] ** 2 + b[1] ** 2),
                    (a[1] * b[0] + a[0] * b[1]) / (b[0] ** 2 + b[1] ** 2),
                ]
            return "Division par 0 !"
        return "ERROR"
