## Exercice 8

# Objectif
L'objectif de cet exercice est de générer une version zippée du projet.

# Réalisation
Pour générer une version zippée du projet, on créé un fichier *setup.py*, contenant des information sur le projet créé.
Pour genérer l'archive, on utilise la commande suivante :

	python3 setup.py sdist
	
On a alors l'arborescence suivante :
	
	.
	├── Calculator
	│   ├── Calculator
	│   │   └── Calculator.py
	│   └── ComplexCalculator.egg-info
	│       ├── dependency_links.txt
	│       ├── PKG-INFO
	│       ├── SOURCES.txt
	│       └── top_level.txt
	├── dist
	│   └── ComplexCalculator-0.0.2.tar.gz
	├── README.md
	├── setup.py
	└── Test
	    ├── ex8.py
	    └── tests.log

